import RPi.GPIO as GPIO

import os
import io
import time
import picamera

import argparse

pin_red = 5
pin_green = 6
pin_blue = 13

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('class_folder', help='TeachableMachine savedmodel')

    parser.add_argument('number', type=int,
                        help='Convert base model to TFLite FlatBuffer, then load model into TFLite Python Interpreter')
    args = parser.parse_args()
    return args


def initStatusLed():
    GPIO.setwarnings(False)
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(pin_red, GPIO.OUT)
    GPIO.setup(pin_green, GPIO.OUT)
    GPIO.setup(pin_blue, GPIO.OUT)

def setRgb(r, g, b):
    global pR, pG, pB

    setGpioValue(pin_red, pR, r)
    setGpioValue(pin_green, pG, g)
    setGpioValue(pin_blue, pB, b)

def setGpioValue(pin, p, v):
    v_ref = v / 255 * 100

    p.start(v_ref)
    
args = parse_args()
initStatusLed()

pR = GPIO.PWM(pin_red, 100)
pG = GPIO.PWM(pin_green, 100)
pB = GPIO.PWM(pin_blue, 100)


setRgb(255, 255, 255)


# Replace this with the path to your image
# image = Image.open('test_photo.jpg')
# Create the in-memory stream
print('start camera')
with picamera.PiCamera() as camera:
    camera.start_preview()

    time.sleep(2)
    count = args.number

    if not os.path.exists(args.class_folder):
        os.mkdir(args.class_folder)

    for filename in camera.capture_continuous(os.path.join(args.class_folder, 'img{counter:03d}.jpg')):
        setRgb(0, 255, 0)
        print('Captured %s' % filename)
        setRgb(255, 255, 255)
        count = count - 1
        if count == 0:
            break;
