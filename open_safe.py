import time
from status_led import StatusLed
from safe_interface import SafeInterface
from image_capture import ImageCapture
from image_classification import ImageClassification

import argparse

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('model_path', help='requires TeachableMachine savedmodel')
    args = parser.parse_args()
    return args

def main():
    args = parse_args()

    status_led = StatusLed()
    status_led.initStatusLed()

    status_led.setStatus(StatusLed.STATUS_INIT)

    safe_interface = SafeInterface()
    safe_interface.initInterface()

    image_classification = ImageClassification(args.model_path, status_led, safe_interface)
    image_classification.loadModel()

    status_led.setStatus(StatusLed.STATUS_WAITING)
    
    image_capture = ImageCapture()
    time.sleep(2)
    print(image_capture)
    image_capture.captureFrames(image_classification.handleImage)

if __name__ == "__main__":
    main()