import RPi.GPIO as GPIO

class StatusLed:
    pin_red = 5
    pin_green = 6
    pin_blue = 13

    STATUS_INIT = (255, 255, 255)
    STATUS_WAITING = (255, 255, 0)
    STATUS_CLOSED = (255, 0, 0)
    STATUS_OPEN = (0, 255, 0)

    def __init__(self):
        self.pR = None
        self.pG = None
        self.pB = None
    

    def initStatusLed(self):
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.pin_red, GPIO.OUT)
        GPIO.setup(self.pin_green, GPIO.OUT)
        GPIO.setup(self.pin_blue, GPIO.OUT)

        self.pR = GPIO.PWM(self.pin_red, 100)
        self.pG = GPIO.PWM(self.pin_green, 100)
        self.pB = GPIO.PWM(self.pin_blue, 100)

    def setStatus(self, status):
        r, g, b = status
        self.setRgb(r, g, b)


    def setRgb(self, r, g, b):
        self.setGpioValue(self.pin_red, self.pR, r)
        self.setGpioValue(self.pin_green, self.pG, g)
        self.setGpioValue(self.pin_blue, self.pB, b)

    def setGpioValue(self, pin, p, v):
        v_ref = v / 255 * 100
        p.start(v_ref)