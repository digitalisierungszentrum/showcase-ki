import time
import RPi.GPIO as GPIO

class SafeInterface:
    PIN_OPEN = 19

    def __init__(self):
        pass

    def initInterface(self):
        GPIO.setup(SafeInterface.PIN_OPEN, GPIO.OUT)
        GPIO.output(SafeInterface.PIN_OPEN, 0)

    def open(self):
        GPIO.output(SafeInterface.PIN_OPEN, 1)
        time.sleep(0.5)
        self.close()

    def close(self):
        GPIO.output(SafeInterface.PIN_OPEN, 0)

    